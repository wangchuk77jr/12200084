def KMP(string, pattern):
    ls = len(string)
    lp = len(pattern)
    pi = [0]*lp
    table = Prefix(pattern, lp, pi)
    print(table)
    i = 0
    j = 0

    index = []
    while i != ls:
        if string[i] == pattern[j]:
            i += 1
            j += 1
        else:
            j = table[j-1]
        if j == lp:
            index.append(i-j)
            j = table[j-1]
        elif j == 0:
            i += 1
    return index


def Prefix(p, lp, pi):
    i, j = 1, 0
    while (i < lp):
        if p[i] == p[j]:
            pi[i] = j+1
            i += 1
            j += 1
        else:
            if(j == 0):
                i += 1
            else:
                j = pi[j-1]
    return pi


string = 'ababcabcabababd'
pattern = 'abbaabacdbb'
x = KMP(string, pattern)
print(x)
