def BinarSearh(arr, n):
    s = 0
    e = len(arr)-1

    while(s <= e):
        mid = (s+e)//2
        if arr[mid] < n:
            s = mid+1
        elif arr[mid] > n:
            e = mid-1
        elif arr[mid] == n:
            return mid
        return -1


arr = list(map(int, input().split()))
n = int(input())
index = BinarSearh(arr, n)
if index == -1:
    print('Element is not present')
else:
    print('Element is present at ', str(index))
