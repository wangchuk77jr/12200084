def rotation(arr, n):
    s = 0
    e = n-1
    while(s <= e):
        mid = s + (e-s)//2
        prev = (mid-1+n) % n
        nex = (mid+1) % n

        if arr[mid] < arr[prev] and arr[mid] <= arr[nex]:
            return mid
        elif arr[mid] < arr[s]:
            e = mid-1
        elif arr[mid] > arr[e]:
            s = mid+1
        else:
            return 0


arr = [7, 9, 11, 12, 15]
n = len(arr)
print(rotation(arr, n))
