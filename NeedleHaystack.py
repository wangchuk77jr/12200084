def KPM(lp, p, s):
    ls = len(s)
    table = [0]*lp
    prefix(p, lp, table)
    i, j = 0, 0
    while i != ls:
        if p[j] == s[i]:
            i += 1
            j += 1
        else:
            if j == 0:
                i += 1
            else:
                j = table[j-1]
        if j == lp:
            print(i-j)
            j = table[j-1]


def prefix(p, lp, table):
    i, j = 1, 0
    while i != lp:
        if p[i] == p[j]:
            table[i] = j+1
            j += 1
            i += 1
        else:
            if j == 0:
                i += 1
            else:
                j = table[j-1]
    return table


if __name__ == '__main__':
    while True:
        try:
            lp = int(input())
            p = input()
            s = input()
            KPM(lp, p, s)
        except(EOFError):
            break
