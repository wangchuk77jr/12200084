def build(s, e, node):
    if (s == e):
        stree[node] = arr[s]
        return arr[s]

    m = (s+e)//2
    left = build(s, m, 2*node+1)
    right = build(m+1, e, 2*node+2)
    stree[node] = left + right
    return stree[node]


def qSum(s, e, l, r, node):
    if l > e or r < s:
        return 0

    if l <= s and r >= e:
        return stree[node]

    mid = (s+e)//2
    left = qSum(s, mid, l, r, 2*node+1)
    right = qSum(mid+1, e, l, r, 2*node+2)

    return left+right


def qUpdate(s, e, node, pos, val):
    if s == e:
        stree[node] = val
        return

    mid = (s+e)//2
    if pos <= mid:
        qUpdate(s, mid, 2*node+1, pos, val)
    else:
        qUpdate(mid+1, e, 2*node+2, pos, val)

    stree[node] = stree[2*node+1]+stree[2*node+2]


def buildTree():
    return build(0, n-1, 0)


def querySum(l, r):
    return qSum(0, n-1, l, r, 0)


def queryUpdate(pos, val):
    return qUpdate(0, n-1, 0, pos, val)


if __name__ == '__main__':
    arr = [1, 3, 4, 2, 8, 7, 5, 6]
    n = len(arr)
    stree = [0]*(4*n)
    buildTree()
    print(stree)
    print(querySum(3, 5))
    queryUpdate(3, 8)
    print(querySum(3, 5))
