def GCD(a, b):
    if b == 0:
        return a
    return GCD(b, a % b)


def MaxGCD(arr):
    ans = 1
    n = len(arr)
    for i in range(n-1):
        for j in range(i+1, n):
            g = GCD(arr[i], arr[j])
            if g > ans:
                ans = g
    return ans


if __name__ == '__main__':
    n = int(input())
    while n:
        arr = list(map(int, input().split()))
        x = MaxGCD(arr)
        print('Maximum GCD is: ', x)
        n -= 1
