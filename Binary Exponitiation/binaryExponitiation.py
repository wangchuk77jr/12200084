import math


def binaryExponentiation(a, b):
    res = 1
    while b:
        if b & 1:
            res *= a
        a *= a
        b >>= 1
    print(res)


x = math.pow(5, 20)
print(x)
binaryExponentiation(5, 20)

mod = 1e9 + 7


def ModbinaryExponentiation(a, b):
    res = 1
    while b:
        if b & 1:
            res *= a
            res %= mod
        a *= a
        a %= mod
        b >>= 1
    print(res)


ModbinaryExponentiation(5, 200)
