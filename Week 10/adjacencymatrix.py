
class Graph:
	# constructor
	def __init__(self, v):
		self.graph = [[0]*v for i in range(v)]

	def add_edge(self, src, dest):
		self.graph[src][dest] = 1
		self.graph[dest][src] = 1

if __name__ == '__main__':
	g = Graph(5) # creating object
	g.add_edge(0, 1)
	g.add_edge(0, 3)
	g.add_edge(0, 2)
	g.add_edge(1, 3)
	g.add_edge(1, 4)
	g.add_edge(2, 3)
	g.add_edge(3, 3)
	g.add_edge(3, 4)
	print(g.graph)