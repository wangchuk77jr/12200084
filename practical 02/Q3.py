# using Iteration
def sorted(arr):
    flag = 0
    for i in range(len(arr)-1):
        if arr[i] < arr[i+1]:
            flag = 1
        else:
            flag = 0
    if flag == 1:
        return True
    else:
        return False


arr = [2, 3, 4, 5]

print(sorted(arr))

# using recursion


def sortRec(arr, i, n):
    if i == n-1:
        return True
    if arr[i] <= arr[i+1] and sortRec(arr, i+1, n):
        return True
    return False


x = sortRec(arr, 0, len(arr))
print(x)
