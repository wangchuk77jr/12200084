# Given an integer n, return the number of prime numbers that are strictly less than n.
# Constraints: 0 <= n <= 5*10^6
# Explanation: There are 4 prime numbers less than 10 [2,3,5,7]
import math


def PrimeSeive(n):
    seive = [1]*(n+1)
    p = 2
    while(p <= math.sqrt(n)):
        if seive[p]:
            for i in range(p*p, n+1, p):
                seive[i] = 0
        p += 1
    return seive


def CountPrime(n):
    seive = PrimeSeive(n)
    print(seive)
    count = 0
    for i in range(2, n):
        if seive[i]:
            count = count+1
    return count


n = int(input())
print(CountPrime(n))
