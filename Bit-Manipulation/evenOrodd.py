def EvenOdd(n):
    lb = n & 1
    if (lb == 0):
        print('Even')
    else:
        print('Odd')


EvenOdd(6)


def countSetBit(n):
    count = 0
    while n:
        if n & 1:
            count += 1
        n >>= 1
    return count


print(countSetBit(10))


def getIthBit(n, i):
    mask = 1 << i
    if n & mask == 0:
        return 0
    else:
        return 1


print(getIthBit(10, 1))
