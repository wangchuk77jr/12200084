def ClearIthBit(number, i):
    
    mask = 1<<i 
    mask = ~ mask
    number = number & mask
    return number

print(ClearIthBit(13,2))