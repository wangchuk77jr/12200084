def decimalToBinary(number):
    binary = list()
    while(number>0):
        lastBit = number & 1
        number = number>>1
        binary.append(str(lastBit)) 
    return "".join(binary[::-1])

print(decimalToBinary(171))