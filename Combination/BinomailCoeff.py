
def BinomialCofficients(n):
    arr = [[0]*(n+1) for i in range(n+1)]
    for i in range(n+1):
        for j in range(i+1):
            if j == 0 or i == j:
                arr[i][j] = 1
            else:
                arr[i][j] = arr[i-1][j] + arr[i-1][j-1]
    return arr


n = 6
x = BinomialCofficients(n)
k = 0
print(x[n][k])


# def binomialCoeff(n):
#     	table = [[0]*(n+1) for i in range(n+1)]
# 	# for row n
# 	for i in range(n+1):
# 		for r in range(i+1): # for column r
# 			if i == r or r == 0:
# 				# nCn or nC0 = 1
# 				table[i][r] = 1
# 			else:
# 				table[i][r] = table[i-1][r-1] + table[i-1][r]

# 	return table

# if __name__ == '__main__':
# 	n = 6
# 	table = binomialCoeff(n)
# 	k = 4
# 	print(table[n][k])
