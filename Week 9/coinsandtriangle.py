def binarySearch(n):
	if n == 1:
		return 1

	l = 1 # min height
	r = n # max height

	height = 0 
	while l <= r:
		mid = (l+r)//2   # current height
		min_count = (mid*(mid+1))//2 # for mid height, min coin required to construct triangle.
		if min_count <= n:
			height = mid
			l = mid+1 # increase min height
		else:
			r = mid-1 # decrease max height

	return height

if __name__ == '__main__':
	t = int(input())
	while t:
		n = int(input())
		print(binarySearch(n))
		t -= 1