def KPM(s, p):
    ls = len(s)
    lp = len(p)
    table = [0]*lp
    prefix(p, lp, table)
    i, j = 1, 1
    count = 0
    index = []
    while i != ls:
        if p[j] == s[i]:
            i += 1
            j += 1
        else:
            if j == 0:
                i += 1
            else:
                j = table[j-1]
        if j == lp:
            count += 1
            index.append(i-j+1)
            j = table[j-1]

    if count != 0:
        print(count)
        for i in index:
            print(i, end=' ')
    else:
        print('Not Found')


def prefix(p, lp, table):
    i, j = 1, 0
    while i != lp:
        if p[i] == p[j]:
            table[i] = j+1
            j += 1
            i += 1
        else:
            if j == 0:
                i += 1
            else:
                j = table[j-1]
    return table


if __name__ == '__main__':
    n = int(input())
    while n:
        A, B = input().split()
        KPM(A, B)
        print('\ \ ')
        n -= 1
